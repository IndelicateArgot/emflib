/// Extract, validate, and parse Encrypted Memo Field (EMF) contents.
///
/// The EMF is a 512 byte blob packed inside shielded transactions.  As such
/// it has the privacy/security properties provided by the zcash encryption
/// protocol.
///
/// This library provides the following functionality:
///
/// Invoke a zcash-cli rpc command to produce a string representing the
/// decrypted contents of a shielded transaction (NOTE: different levels of
/// privilege are possible).
///
/// Convert the produced string into a Rust type using the serge-json
///
/// Interpret the 
///
/// Validate that such an extracted JSON is a valid ZcCliTransaction
/// instance.

extern crate serde;
extern crate serde_json;
#[macro_use]
extern crate serde_derive;

use serde_json::Error;
use std::process::{Command, Stdio, Output};

#[derive(Debug, Serialize, Deserialize)]
pub struct Amount { value: u64 } // 0 <= value <= 21000000000000 
/// I am using this:
/// https://zcash-rpc.github.io/z_listreceivedbyaddress.html
/// as my template for this:

#[derive(Debug, Serialize, Deserialize)]
pub struct ZcTransactions {
    Transactions: Vec<ZcTransaction>
}

#[derive(Debug, Serialize, Deserialize)]
pub struct ZcTransaction {
    txid:     String,
    amount:   Amount, 
    memo:     MemoField,
    outindex: u64,    
    change:   bool
}

#[derive(Debug, Serialize, Deserialize)]
pub struct MemoField { }
impl MemoField {
    pub fn new() -> MemoField {
        MemoField {}
    }
}
   
pub fn deserialize_transactions(serial_txs: &str)
        -> Result<Vec<ZcTransaction>, Error> {
    println!("{:?}", serial_txs);
    //let sj: ZcTransactions = serde_json::from_str(serial_txs).expect(
    //    "Failed to deserialize using from_str");
    let sj: Vec<ZcTransaction> = serde_json::from_str(serial_txs).expect(
        "Failed to deserialize using from_str");
    println!("{:?}", sj);
    Ok(sj)
}

pub fn call_z_listreceivedbyaddress(address: &str)
        -> Output {
    let result = Command::new("zcash-cli")
                 .arg("z_listreceivedbyaddress")
                 .arg(address)
                 .stdout(Stdio::piped())
                 .spawn()
                 .expect("failed to execute zcash-cli");
    let output = result
                 .wait_with_output()
                 .expect("Failed to wait on zcash-cli");
    if !output.status.success() { panic!("Oh no!") };
    output 
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn it_works() {
          assert_eq!(2+2, 4);
//        assert_eq!(deserialize_transactions(
//            "ztestsapling\
//            1fpf39y8htnhepvhzrw5ht5ma69gkwnnv9vnz6\
//            yxeaqmf7u6q6qawmfedtpmpjgzhq5zmvfdx533"
//        ), ());
    }
}
    
